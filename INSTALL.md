#  camera-observer

## Установка
1. Склонировать проект и зайти в папку `/docker`
1. При необходимости создать файл `/docker/.env` и переопределить переменные из `/docker/.env.dist`
1. Запустить контейнеры `docker-compose up -d`
1. Зайти в контейнер `docker-compose exec --user=app app bash`
1. Сконфигурировать приложение (см. раздел [Настройка](#configuration))
1. Установить зависимости `composer install`

## <a name="configuration"></a> Настройка
1. Создать файл `.env.local` и установить значения переменных :
```
CAMERA_IMAGE_HOST=http://host:port
CAMERA_USERNAME=admin
CAMERA_PASSWORD=password

TELEGRAM_BOT_TOKEN=xxxxxxxxx:yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
TELEGRAM_BOT_NAME=bot_name
TELEGRAM_CHAT_ID=-111111111

CHECK_ZONES_CONFIG='269,89,5,3,Первый котёл,config/masks/boiler1_red.jpg|52,162,9,6,Второй котёл,config/masks/boiler2_red.jpg'
IMAGE_ANALYZER_SENSIVITY_INDEX=0.5
```

Переменная `CHECK_ZONES_CONFIG` содержит список конфигураций наблюдаемых на изображении зон, разделённых символом `|`. Конфигурация одной зоны состоит из списка параметров, разделённых запятой. Последовательность параметров наблюдаемой зоны:
- координата X левого верхнего угла наблюдаемой зоны;
- координа Y левого верхнего угла наблюдаемой зоны;
- ширина наблюдаемой зоны в пикселях;
- высота наблюдаемой зоны в пикселях;
- название наблюдаемой зоны;
- путь к изображению, описывающему состояние, при котором приложение должно уведомить пользователя о наступлении события (размер изображения должен соответствовать размеру наблюдаемой зоны);

## Использование
1. Зайти в контейнер `app`
1. Выполнить команду `php bin/console app:camera:check`
