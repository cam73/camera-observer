<?php

namespace Tests\Functional\Camera;

use Application\Component\Camera\Resource\CameraResourceProvider;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class ImageProviderTest
 * @package Tests\Functional\Camera
 *
 * @covers \Application\Component\Camera\Resource\CameraResourceProvider
 */
class CameraResourceProviderTest extends KernelTestCase
{
    /**
     * @test
     */
    public function getImageContent()
    {
        self::bootKernel();

        $imageContent = $this->getImageProvider()->getResource();
        expect(gettype($imageContent))->equals('resource');
    }

    private function getImageProvider(): CameraResourceProvider
    {
        return self::$container->get('test_camera_image_provider');
    }
}
