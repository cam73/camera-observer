<?php

namespace Tests\Component\Trigger;

use Application\Component\Trigger\TriggerReport;
use PHPUnit\Framework\TestCase;

class TriggerReportTest extends TestCase
{
    public function test()
    {
        $triggerReport1 = new TriggerReport('test1', 'test2', false);
        $triggerReport2 = new TriggerReport('test3', 'test4', true);

        expect($triggerReport1->getEquipmentDescription())->equals('test1');
        expect($triggerReport1->getTriggerDescription())->equals('test2');
        expect($triggerReport1->isTriggered())->false();

        expect($triggerReport2->getEquipmentDescription())->equals('test3');
        expect($triggerReport2->getTriggerDescription())->equals('test4');
        expect($triggerReport2->isTriggered())->true();
    }
}
