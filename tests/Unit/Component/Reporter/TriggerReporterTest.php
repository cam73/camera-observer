<?php

namespace Tests\Component\Reporter;

use Application\Component\Reporter\TriggerReporter;
use Assert\InvalidArgumentException;
use Domain\TriggerReportInterface;
use Domain\TriggerSubscriberInterface;
use PHPUnit\Framework\TestCase;

class TriggerReporterTest extends TestCase
{
    /**
     * @test
     */
    public function report()
    {
        $triggerReport = $this->prophesize(TriggerReportInterface::class);
        $context = ['context'];

        $subscriber1 = $this->prophesize(TriggerSubscriberInterface::class);
        $subscriber1->notify($triggerReport, $context)->shouldBeCalled();

        $subscriber2 = $this->prophesize(TriggerSubscriberInterface::class);
        $subscriber2->notify($triggerReport, $context)->shouldBeCalled();

        $reporter = new TriggerReporter([$subscriber1->reveal(), $subscriber2->reveal()]);
        $reporter->report($triggerReport->reveal(), $context);
    }

    /**
     * @test
     */
    public function failedConstruct()
    {
        $this->expectException(InvalidArgumentException::class);

        new TriggerReporter(['test']);
    }
}
