<?php

namespace Tests\Component\ImageAnalyzer\ImageId;

use Application\Component\Image\Image;
use Application\Component\ImageAnalyzer\ImageId\ImageId;
use Assert\AssertionFailedException;
use PHPUnit\Framework\TestCase;

class ImageIdTest extends TestCase
{
    /**
     * @test
     * @throws AssertionFailedException
     */
    public function successfulConstruct()
    {
        $imageId = new ImageId($this->getImage(), 2, 2);
        expect($imageId->getId())->equals('00200 01200 10200 11200');
        expect($imageId->explodeId())->equals(['00200', '01200', '10200', '11200']);
    }

    /**
     * @test
     * @dataProvider invalidConstructDataProvider
     *
     * @param $image
     * @param int $width
     * @param int $height
     * @param string $expectedMessage
     * @throws AssertionFailedException
     */
    public function invalidConstruct($image, int $width, int $height, string $expectedMessage)
    {
        $this->expectException(AssertionFailedException::class);
        $this->expectExceptionMessage($expectedMessage);
        new ImageId($image, $width, $height);
    }

    public function invalidConstructDataProvider(): array
    {
        return [
            'invalid $width' => [
                'image' => $this->getImage(),
                'width' => 0,
                'height' => 2,
                'expectedMessage' => 'The $width must be greater than zero',
            ],
            'invalid $height' => [
                'image' => $this->getImage(),
                'width' => 2,
                'height' => 0,
                'expectedMessage' => 'The $height must be greater than zero',
            ],
        ];
    }

    /**
     * @test
     * @dataProvider diffDataProvider
     *
     * @param array $comparedImageIdArray
     * @param float $expectedDiff
     * @throws AssertionFailedException
     */
    public function diff(array $comparedImageIdArray, float $expectedDiff)
    {
        /** @var ImageId $comparedImageId */
        $comparedImageId = $this->prophesize(ImageId::class);
        $comparedImageId->explodeId()->willReturn($comparedImageIdArray);

        $imageId = new ImageId($this->getImage(), 2, 2);
        expect($imageId->diff($comparedImageId->reveal()))->equals($expectedDiff);
    }

    public function diffDataProvider(): array
    {
        return [
            'full equivalent' => [
                'imageId' => ['00200', '01200', '10200', '11200'],
                'expectedDiff' => 1,
            ],
            'half similarity' => [
                'imageId' => ['00200', '01200', '00000', '00000'],
                'expectedDiff' => 0.5,
            ],
            'not similarity' => [
                'imageId' => ['00000', '00000', '00000', '00000'],
                'expectedDiff' => 0,
            ],
        ];
    }

    private function getImage()
    {
        $imageResource = imagecreatetruecolor (2, 2);
        imagefill ( $imageResource , 0, 0,  imagecolorallocate($imageResource, 255, 0, 0));

        return new Image($imageResource);
    }
}
