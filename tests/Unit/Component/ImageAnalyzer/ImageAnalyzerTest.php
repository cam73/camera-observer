<?php

namespace Tests\Component\ImageAnalyzer;

use Application\Component\Image\Image;
use Application\Component\ImageAnalyzer\ImageAnalyzeParams;
use Application\Component\ImageAnalyzer\ImageAnalyzer;
use Assert\AssertionFailedException;
use PHPUnit\Framework\TestCase;

class ImageAnalyzerTest extends TestCase
{
    /**
     * @test
     * @dataProvider analyzeDiffProvider
     *
     * @param string $resourcePath
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @param string $maskPath
     * @param float $expected
     * @throws AssertionFailedException
     */
    public function analyzeDiff(
        string $resourcePath,
        int $x,
        int $y,
        int $width,
        int $height,
        string $maskPath,
        float $expected
    )
    {
        $resource = new Image(imagecreatefromjpeg($resourcePath));
        $mask = new Image(imagecreatefromjpeg($maskPath));
        $params = new ImageAnalyzeParams($resource, $x, $y, $width, $height, $mask);

        $analyzer = new ImageAnalyzer();
        $result = $analyzer->analyzeDiff($params);
        expect($result)->equals($expected);
    }

    public function analyzeDiffProvider(): array
    {
        return [
            'Maximum similarity' => [
                'resourcePath' => 'tests/Fixtures/images/red_led_snapshot.jpg',
                'x' => 52,
                'y' => 162,
                'width' => 9,
                'height' => 6,
                'maskPath' => 'tests/Fixtures/images/red_led.jpg',
                'expected' => 0.64814814814815,
            ],
            'Minimum similarity' => [
                'resourcePath' => 'tests/Fixtures/images/red_led_snapshot.jpg',
                'x' => 52,
                'y' => 162,
                'width' => 9,
                'height' => 6,
                'maskPath' => 'tests/Fixtures/images/yellow_led.jpg',
                'expected' => 0.48148148148148,
            ],
        ];
    }

    /**
     * @test
     */
    public function getImageHash()
    {
        $resource = new Image(imagecreatefromjpeg('tests/Fixtures/images/red_led.jpg'));
        $analyzer = new ImageAnalyzer();
        $result = $analyzer->getImageHash($resource);
        expect($result)->equals('e43b522a536a12014c957bd69eba7475');
    }
}
