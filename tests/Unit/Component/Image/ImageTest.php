<?php

namespace Tests\Component\Image;

use Application\Component\Image\Image;
use Assert\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class ImageTest extends TestCase
{
    /**
     * @test
     */
    public function successfulConstruct()
    {
        $resource = imagecreate(1, 1);
        $image = new Image($resource);
        expect($this->getImageHash($image->getImageResource()))
            ->equals($this->getImageHash($resource));
    }

    /**
     * @test
     */
    public function failedConstruct()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The $imageResource is not resource');

        new Image('nonResource');
    }

    /**
     * @param resource $resource
     * @return string
     */
    private function getImageHash($resource): string
    {
        ob_start();
        @imagepng($resource);
        $content = ob_get_clean();

        return $content;
    }
}
