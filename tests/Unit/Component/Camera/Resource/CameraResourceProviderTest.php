<?php

namespace Tests\Component\Camera\Resource;

use Application\Component\Camera\Resource\CameraResourceProvider;
use Application\Component\Camera\Resource\CameraResourceProviderException;
use Http\Client\Exception\TransferException;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class CameraResourceProviderTest extends TestCase
{
    public function test()
    {
        $imageContent = file_get_contents('tests/Fixtures/images/red_led_snapshot.jpg');

        $response = $this->prophesize(ResponseInterface::class);
        $response->getBody()->willReturn($imageContent)->shouldBeCalled();

        $httpClient = $this->prophesize(ClientInterface::class);
        $httpClient->sendRequest(Argument::any())->willReturn($response->reveal())->shouldBeCalled();

        $provider = new CameraResourceProvider($httpClient->reveal());
        $resource = $provider->getResource();
        $cachedResource = $provider->getCashResource();

        expect($resource)->resource();
        expect($this->getImageHash($resource))
            ->equals($this->getImageHash(imagecreatefromstring($imageContent)));
        expect($this->getImageHash($cachedResource))
            ->equals($this->getImageHash(imagecreatefromstring($imageContent)));
    }

    /**
     * @test
     */
    public function throwsExceptionOnUnsupportedImage()
    {
        $this->expectException(CameraResourceProviderException::class);
        $this->expectExceptionMessage('The image type is unsupported, the data is not in a recognised format, or the image is corrupt and cannot be loaded.');

        $response = $this->prophesize(ResponseInterface::class);
        $response->getBody()->willReturn('unsupported content');

        $httpClient = $this->prophesize(ClientInterface::class);
        $httpClient->sendRequest(Argument::any())->willReturn($response->reveal());

        (new CameraResourceProvider($httpClient->reveal()))->getResource();
    }

    /**
     * @test
     */
    public function throwsExceptionOnHttpProblem()
    {
        $this->expectException(CameraResourceProviderException::class);
        $this->expectExceptionMessage('exception message');

        $httpClient = $this->prophesize(ClientInterface::class);
        $httpClient->sendRequest(Argument::any())
            ->willThrow(new TransferException('exception message'))
        ;

        (new CameraResourceProvider($httpClient->reveal()))->getResource();
    }

    /**
     * @param resource $resource
     * @return string
     */
    private function getImageHash($resource): string
    {
        ob_start();
        @imagepng($resource);
        $content = ob_get_clean();

        return $content;
    }
}
