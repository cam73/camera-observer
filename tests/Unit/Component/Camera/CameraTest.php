<?php

namespace Tests\Component\Camera;

use Application\Component\Camera\Camera;
use Application\Component\Camera\Resource\CameraResourceProvider;
use PHPUnit\Framework\TestCase;

class CameraTest extends TestCase
{
    public function test()
    {
        $resourceProvider = $this->prophesize(CameraResourceProvider::class);
        $resource = imagecreate(1, 1);
        $resourceProvider->getCashResource()->willReturn($resource);

        $camera = new Camera('cameraTitle', $resourceProvider->reveal());
        expect($camera->getImage()->getImageResource())->equals($resource);
        expect($camera->getTitle())->equals('cameraTitle');
    }
}
