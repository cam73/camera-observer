<?php

namespace Tests\Component\TelegramMediaMessenger;

use Application\Component\MediaMessenger\MediaMessage;
use Application\Component\TelegramApiClient\TelegramApiClient;
use Application\Component\TelegramMediaMessenger\TelegramMediaMessenger;
use PHPUnit\Framework\TestCase;

class TelegramMediaMessengerTest extends TestCase
{
    /**
     * @test
     */
    public function sendMediaMessage()
    {
        $apiClient = $this->prophesize(TelegramApiClient::class);
        $apiClient->sendPhoto('imagePath', 'text')->shouldBeCalled();

        $messenger = new TelegramMediaMessenger($apiClient->reveal());
        $messenger->sendMediaMessage(new MediaMessage('text', 'imagePath'));
    }
}
