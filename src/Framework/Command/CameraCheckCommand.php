<?php

namespace Framework\Command;

use Application\Feature\CameraObserver\UseCase\ObserveAndNoticeByEvent\Interactor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CameraCheckCommand extends Command
{
    protected static $defaultName = 'app:camera:check';

    private Interactor $interactor;

    public function __construct(
        Interactor $interactor
    ) {
        $this->interactor = $interactor;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Started.');

        $this->interactor->observeAndNoticeByEvent();

        $output->writeln('Finished.');
        return 0;
    }
}
