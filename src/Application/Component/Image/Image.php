<?php

namespace Application\Component\Image;

use Assert\Assertion;

class Image
{
    /** @var resource */
    private $imageResource;

    /**
     * @param resource $imageResource
     */
    public function __construct($imageResource)
    {
        Assertion::isResource($imageResource, 'The $imageResource is not resource');
        $this->imageResource = $imageResource;
    }

    /**
     * @return resource
     */
    public function getImageResource()
    {
        return $this->imageResource;
    }
}
