<?php

namespace Application\Component\TelegramMediaMessenger;

use Application\Component\MediaMessenger\MediaMessage;
use Application\Component\MediaMessenger\MediaMessengerInterface;
use Application\Component\TelegramApiClient\TelegramApiClient;

class TelegramMediaMessenger implements MediaMessengerInterface
{
    private TelegramApiClient $telegramApiClient;

    public function __construct(TelegramApiClient $telegramApiClient)
    {
        $this->telegramApiClient = $telegramApiClient;
    }

    public function sendMediaMessage(MediaMessage $mediaMessage): void
    {
        $this->telegramApiClient->sendPhoto($mediaMessage->getImagePath(), $mediaMessage->getText());
    }
}
