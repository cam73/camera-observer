<?php

namespace Application\Component\Trigger;

use Domain\TriggerReportInterface;

class TriggerReport implements TriggerReportInterface
{
    private string $equipmentDescription;

    private string $triggerDescription;

    private bool $isTriggered;

    public function __construct(
        string $equipmentDescription,
        string $triggerDescription,
        bool $isTriggered
    )
    {
        $this->equipmentDescription = $equipmentDescription;
        $this->triggerDescription = $triggerDescription;
        $this->isTriggered = $isTriggered;
    }

    public function getEquipmentDescription(): string
    {
        return $this->equipmentDescription;
    }

    public function getTriggerDescription(): string
    {
        return $this->triggerDescription;
    }

    public function isTriggered(): bool
    {
        return $this->isTriggered;
    }
}
