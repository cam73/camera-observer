<?php

namespace Application\Component\Reporter;

use Assert\Assertion;
use Domain\TriggerReporterInterface;
use Domain\TriggerReportInterface;
use Domain\TriggerSubscriberInterface;

class TriggerReporter implements TriggerReporterInterface
{
    /** @var TriggerSubscriberInterface[] */
    private array $subscribers;

    /**
     * TriggerReporter constructor.
     * @param TriggerSubscriberInterface[] $subscribers
     */
    public function __construct(array $subscribers)
    {
        Assertion::allIsInstanceOf($subscribers, TriggerSubscriberInterface::class);
        $this->subscribers = $subscribers;
    }

    public function report(TriggerReportInterface $triggerReport, array $context): void
    {
        foreach ($this->subscribers as $subscriber) {
            $subscriber->notify($triggerReport, $context);
        }
    }
}
