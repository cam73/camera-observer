<?php

namespace Application\Component\ImageEquipment;

use Application\Component\Image\Image;
use Domain\EquipmentInterface;

interface ImageEquipmentInterface extends EquipmentInterface
{
    public function getImage(): Image;
}
