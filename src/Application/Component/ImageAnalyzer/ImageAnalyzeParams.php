<?php

namespace Application\Component\ImageAnalyzer;

use Application\Component\Image\Image;
use Assert\Assertion;
use Assert\AssertionFailedException;

class ImageAnalyzeParams
{
    private Image $sourceImage;

    private int $x;

    private int $y;

    private int $width;

    private int $height;

    private Image $maskImage;

    /**
     * ImageAnalyzeParams constructor.
     * @param Image $sourceImage
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @param Image $maskImage
     * @throws AssertionFailedException
     */
    public function __construct(
        Image $sourceImage,
        int $x,
        int $y,
        int $width,
        int $height,
        Image $maskImage
    ) {
        Assertion::greaterOrEqualThan($x, 0, 'The X less than 0');
        Assertion::greaterOrEqualThan($y, 0, 'The Y less than 0');
        Assertion::greaterOrEqualThan($width, 0, 'The width less than 0');
        Assertion::greaterOrEqualThan($height, 0, 'The height less than 0');

        $this->sourceImage = $sourceImage;
        $this->x = $x;
        $this->y = $y;
        $this->width = $width;
        $this->height = $height;
        $this->maskImage = $maskImage;
    }

    public function getSourceImage(): Image
    {
        return $this->sourceImage;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getMaskImage(): Image
    {
        return $this->maskImage;
    }
}
