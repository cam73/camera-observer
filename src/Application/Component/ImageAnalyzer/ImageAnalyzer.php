<?php

namespace Application\Component\ImageAnalyzer;

use Application\Component\ImageAnalyzer\ImageId\ImageId;
use Application\Component\Image\Image;
use Assert\AssertionFailedException;

class ImageAnalyzer
{
    /**
     * @param ImageAnalyzeParams $params
     * @return float
     * @throws AssertionFailedException
     */
    public function analyzeDiff(ImageAnalyzeParams $params): float
    {
        $zone = new Image(imagecreate($params->getWidth(), $params->getHeight()));
        imagecopy(
            $zone->getImageResource(),
            $params->getSourceImage()->getImageResource(),
            0,
            0,
            $params->getX(),
            $params->getY(),
            $params->getWidth(),
            $params->getHeight()
        );

        $zoneImageId = new ImageId($zone, $params->getWidth(), $params->getHeight());
        $maskImageId = new ImageId($params->getMaskImage(), $params->getWidth(), $params->getHeight());

        return $zoneImageId->diff($maskImageId);
    }

    public function getImageHash(Image $image): string
    {
        ob_start();
        imagepng($image->getImageResource());
        $content =  ob_get_clean();

        return hash('md5', $content);
    }
}
