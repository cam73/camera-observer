<?php

namespace Application\Component\ImageAnalyzer\ImageId;

use Application\Component\Image\Image;
use Assert\Assertion;
use Assert\AssertionFailedException;

class ImageId
{
    private string $id;

    /**
     * @param Image $image
     * @param int $width
     * @param int $height
     * @throws AssertionFailedException
     */
    public function __construct(Image $image, int $width, int $height)
    {
        Assertion::greaterThan($width, 0, 'The $width must be greater than zero');
        Assertion::greaterThan($height, 0, 'The $height must be greater than zero');
        $this->id = $this->calcId($image, $width, $height);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function explodeId(): array
    {
        return explode(' ', $this->id);
    }

    public function diff(ImageId $comparedImageId): float
    {
        $result = 0;

        $imageIdArray1 = $this->explodeId();
        $imageIdArray2 = $comparedImageId->explodeId();

        foreach ($imageIdArray1 as $bit) {
            if (in_array($bit, $imageIdArray2)) {
                $result++;
            }
        }

        return $result / ((count($imageIdArray1) + count($imageIdArray2)) / 2);
    }

    private function calcId(Image $image, int $width, int $height): string
    {
        $colorMap = [];
        $averageColor = 0;
        $result = [];

        //Заполняем маску и вычисляем базовый цвет
        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $color = imagecolorat($image->getImageResource(), $x, $y);
                $color = imagecolorsforindex($image->getImageResource(), $color);
                $colorMap[$x][$y] = 0.212671 * $color['red'] + 0.715160 * $color['green'] + 0.072169 * $color['blue'];
                $averageColor += $colorMap[$x][$y];
            }
        }

        $averageColor /= 400;

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $result[] = ($x < 10 ? $x : chr($x + 97)) . ($y < 10 ? $y : chr($y + 97)) . round(
                        2 * $colorMap[$x][$y] / $averageColor
                    );
            }
        }

        return join(' ',$result);
    }
}
