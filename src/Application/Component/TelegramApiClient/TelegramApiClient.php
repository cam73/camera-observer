<?php

namespace Application\Component\TelegramApiClient;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

class TelegramApiClient
{
    private Telegram $botApi;

    private int $chatId;

    /**
     * TelegramApiClient constructor.
     * @param Telegram $botApi
     * @param int $chatId
     */
    public function __construct(Telegram $botApi, int $chatId)
    {
        $this->botApi = $botApi;
        $this->chatId = $chatId;
    }

    public function sendPhoto(string $imagePath, string $caption): void
    {
        Request::sendPhoto(['chat_id' => $this->chatId, 'photo' => $imagePath, 'caption' => $caption]);
    }
}

