<?php

namespace Application\Component\Camera\Resource;

use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

use function imagecreatefromstring;

class CameraResourceProvider
{
    private ClientInterface $httpClient;

    /** @var resource */
    private $lastResource;

    public function __construct(
        ClientInterface $httpClient
    ) {
        $this->httpClient = $httpClient;
    }

    /**
     * @return resource
     * @throws CameraResourceProviderException
     */
    public function getResource()
    {
        $resource = @imagecreatefromstring($this->sendRequest()->getBody());

        if (false === $resource) {
            throw new CameraResourceProviderException('The image type is unsupported, the data is not in a recognised format, or the image is corrupt and cannot be loaded.');
        }

        return $resource;
    }

    /**
     * @return resource
     */
    public function getCashResource()
    {
        if (is_null($this->lastResource)) {
            $this->lastResource = $this->getResource();
        }

        return $this->lastResource;
    }

    private function sendRequest(): ResponseInterface
    {
        try {
            $request = new Request('GET', '/image.jpg');

            return $this->httpClient->sendRequest($request);

        } catch (ClientExceptionInterface $e) {
            throw new CameraResourceProviderException($e->getMessage());
        }
    }
}
