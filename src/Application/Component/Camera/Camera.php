<?php

namespace Application\Component\Camera;

use Application\Component\Camera\Resource\CameraResourceProvider;
use Application\Component\Image\Image;
use Application\Component\ImageEquipment\ImageEquipmentInterface;

class Camera implements ImageEquipmentInterface
{
    private string $title;

    private CameraResourceProvider $resourceProvider;

    public function __construct(string $title, CameraResourceProvider $resourceProvider)
    {
        $this->title = $title;
        $this->resourceProvider = $resourceProvider;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getImage(): Image
    {
        return new Image($this->resourceProvider->getCashResource());
    }
}
