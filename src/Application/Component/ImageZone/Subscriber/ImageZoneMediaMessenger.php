<?php

namespace Application\Component\ImageZone\Subscriber;

use Application\Component\ImageZone\Formatter\ImageZoneFormatter;
use Application\Component\ImageZone\Trigger\ImageZoneContext;
use Application\Component\MediaMessenger\MediaMessage;
use Application\Component\MediaMessenger\MediaMessengerInterface;
use Domain\TriggerReportInterface;
use Domain\TriggerSubscriberInterface;

class ImageZoneMediaMessenger implements TriggerSubscriberInterface
{
    private string $cacheDir;

    private MediaMessengerInterface $mediaMessenger;

    private ImageZoneFormatter $formatter;

    public function __construct(
        string $cacheDir,
        MediaMessengerInterface $mediaMessenger,
        ImageZoneFormatter $formatter
    )
    {
        $this->cacheDir = $cacheDir;
        $this->mediaMessenger = $mediaMessenger;
        $this->formatter = $formatter;
    }

    public function notify(TriggerReportInterface $triggerReport, array $context): void
    {
        if (!$triggerReport->isTriggered()) {
            return;
        }

        $imageZoneContext = new ImageZoneContext($context);

        $imagePath = sprintf('%s/camera.%s.jpg', $this->cacheDir, $imageZoneContext->getImageHash());
        imagejpeg($imageZoneContext->getImage()->getImageResource(), $imagePath, 100);

        $mediaMessage = new MediaMessage(
            $this->formatter->format(
                $triggerReport->getEquipmentDescription(),
                $triggerReport->getTriggerDescription(),
                ),
            $imagePath
        );

        $this->mediaMessenger->sendMediaMessage($mediaMessage);
    }
}
