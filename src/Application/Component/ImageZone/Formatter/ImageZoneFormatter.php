<?php

namespace Application\Component\ImageZone\Formatter;

class ImageZoneFormatter
{
    public function format(string $equipmentDescription, string $triggerDescription): string
    {
        return sprintf(
            'Оборудование "%s". Зона "%s" требует внимания.',
            $equipmentDescription,
            $triggerDescription
        );
    }
}
