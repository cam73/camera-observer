<?php

namespace Application\Component\ImageZone\Trigger;

use Application\Component\Image\Image;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageZoneContext
{
    private array $options;

    public function __construct(array $options)
    {
        $resolver = new OptionsResolver();
        $resolver->setRequired('image');
        $resolver->setRequired('imageHash');
        $resolver->setAllowedTypes('image', Image::class);
        $resolver->setAllowedTypes('imageHash', 'string');
        $this->options = $resolver->resolve($options);
    }

    public function getImage(): Image
    {
        return $this->options['image'];
    }

    public function getImageHash(): string
    {
        return $this->options['imageHash'];
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
