<?php

namespace Application\Component\ImageZone\Trigger;

use Application\Component\Image\Image;
use Assert\Assertion;
use Assert\AssertionFailedException;

class ImageZoneConfig
{
    private int $x;

    private int $y;

    private int $width;

    private int $height;

    private int $sensitivityIndex;

    private Image $maskImage;

    /**
     * ImageZoneConfig constructor.
     * @param int $x
     * @param int $y
     * @param int $width
     * @param int $height
     * @param int $sensitivityIndex
     * @param string $maskPath
     * @throws AssertionFailedException
     */
    public function __construct(
        int $x,
        int $y,
        int $width,
        int $height,
        int $sensitivityIndex,
        string $maskPath
    )
    {
        Assertion::greaterOrEqualThan($x, 0, 'The X less than 0');
        Assertion::greaterOrEqualThan($y, 0, 'The Y less than 0');
        Assertion::greaterOrEqualThan($width, 0, 'The width less than 0');
        Assertion::greaterOrEqualThan($height, 0, 'The height less than 0');
        Assertion::range($sensitivityIndex, 0, 100, 'The sensitivityIndex is not in the range from 0 to 100');

        Assertion::file($maskPath, sprintf('Mask file "%s" not found.', $maskPath));

        $this->maskImage = new Image(@imagecreatefromstring(file_get_contents($maskPath)));

        $this->x = $x;
        $this->y = $y;
        $this->width = $width;
        $this->height = $height;
        $this->sensitivityIndex = $sensitivityIndex;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getSensitivityIndex(): int
    {
        return $this->sensitivityIndex;
    }

    public function getMaskImage(): Image
    {
        return $this->maskImage;
    }
}
