<?php

namespace Application\Component\ImageZone\Trigger;

use Application\Component\ImageAnalyzer\ImageAnalyzeParams;
use Application\Component\ImageAnalyzer\ImageAnalyzer;
use Application\Component\ImageEquipment\ImageEquipmentInterface;
use Application\Component\Reporter\TriggerReporter;
use Application\Component\Trigger\TriggerReport;
use Assert\AssertionFailedException;
use Domain\TriggerInterface;

class ImageZoneTrigger implements TriggerInterface
{
    private string $triggerDescription;

    private ImageEquipmentInterface $equipment;

    private ImageZoneConfig $config;

    private ImageAnalyzer $imageAnalyzer;

    private TriggerReporter $reporter;

    public function __construct(
        string $triggerDescription,
        ImageEquipmentInterface $equipment,
        ImageZoneConfig $config,
        ImageAnalyzer $imageAnalyzer,
        TriggerReporter $reporter
    ) {
        $this->triggerDescription = $triggerDescription;
        $this->equipment = $equipment;
        $this->config = $config;
        $this->imageAnalyzer = $imageAnalyzer;
        $this->reporter = $reporter;
    }

    /**
     * @throws AssertionFailedException
     */
    public function execute(): void
    {
        $image = $this->equipment->getImage();

        $diff = $this->imageAnalyzer->analyzeDiff(
            new ImageAnalyzeParams(
                $image,
                $this->config->getX(),
                $this->config->getY(),
                $this->config->getWidth(),
                $this->config->getHeight(),
                $this->config->getMaskImage()
            )
        );

        $isTriggered = ($diff >= ($this->config->getSensitivityIndex() / 100));

        $report = new TriggerReport(
            $this->equipment->getTitle(),
            $this->triggerDescription,
            $isTriggered
        );

        $context = new ImageZoneContext(
            [
                'image' => $image,
                'imageHash' => $this->imageAnalyzer->getImageHash($image),
            ]
        );

        $this->reporter->report($report, $context->getOptions());
    }
}
