<?php

namespace Application\Component\MediaMessenger;

class MediaMessage
{
    private string $text;
    private string $imagePath;

    public function __construct(string $text, string $imagePath)
    {
        $this->text = $text;
        $this->imagePath = $imagePath;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getImagePath(): string
    {
        return $this->imagePath;
    }
}
