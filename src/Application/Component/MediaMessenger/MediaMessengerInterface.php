<?php

namespace Application\Component\MediaMessenger;

interface MediaMessengerInterface
{
    public function sendMediaMessage(MediaMessage $mediaMessage): void;
}
