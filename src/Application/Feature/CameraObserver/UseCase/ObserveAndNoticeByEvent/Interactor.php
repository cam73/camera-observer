<?php

namespace Application\Feature\CameraObserver\UseCase\ObserveAndNoticeByEvent;

use Domain\TriggerHandler;

class Interactor
{
    private TriggerHandler $triggerHandler;

    public function __construct(TriggerHandler $triggerHandler)
    {
        $this->triggerHandler = $triggerHandler;
    }

    public function observeAndNoticeByEvent(): void
    {
        $this->triggerHandler->handle();
    }
}
