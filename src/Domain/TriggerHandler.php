<?php

namespace Domain;

use Assert\Assertion;

class TriggerHandler
{
    /** @var TriggerInterface[] */
    private array $triggers;

    /**
     * TriggerHandler constructor.
     * @param TriggerInterface[] $triggers
     */
    public function __construct(array $triggers)
    {
        Assertion::allIsInstanceOf($triggers, TriggerInterface::class);
        $this->triggers = $triggers;
    }

    public function handle()
    {
        foreach ($this->triggers as $trigger) {
            $trigger->execute();
        }
    }
}
