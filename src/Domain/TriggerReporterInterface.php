<?php

namespace Domain;

interface TriggerReporterInterface
{
    public function report(TriggerReportInterface $triggerReport, array $context): void;
}
