<?php

namespace Domain;

interface TriggerSubscriberInterface
{
    public function notify(TriggerReportInterface $triggerReport, array $context): void;
}
