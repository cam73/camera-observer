<?php

namespace Domain;

interface TriggerReportInterface
{
    public function getEquipmentDescription(): string;

    public function getTriggerDescription(): string;

    public function isTriggered(): bool;
}
