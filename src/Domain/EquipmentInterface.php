<?php

namespace Domain;

interface EquipmentInterface
{
    public function getTitle(): string;
}
