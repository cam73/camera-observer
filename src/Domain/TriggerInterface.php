<?php

namespace Domain;

interface TriggerInterface
{
    public function execute(): void;
}
